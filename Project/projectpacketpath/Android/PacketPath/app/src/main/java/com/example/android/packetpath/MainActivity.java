package com.example.android.packetpath;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private EditText file1;
    private EditText file2;
    private TextView result1;
    private TextView result2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        file1 = (EditText) findViewById(R.id.file1);
        file2 = (EditText) findViewById(R.id.file2);
        result1 = (TextView) findViewById(R.id.result1);
        result2 = (TextView) findViewById(R.id.result2);
    }

    public void compare (View view){
        String filename1 = file1.getText().toString();
        String filename2 = file2.getText().toString();
        JSONObject o = new JSONObject();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringBuilder stringBuilder = new StringBuilder("http://localhost:8084/WebApplication1/webresources/generic/");
        stringBuilder.append(filename1);
        stringBuilder.append("/");
        stringBuilder.append(filename2);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                stringBuilder.toString(),
                o,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {

                            try {
                                result1.setText(response.getString("result"));
                                result2.setText(response.getString("time"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        queue.add(jsonObjectRequest);
    }


}
