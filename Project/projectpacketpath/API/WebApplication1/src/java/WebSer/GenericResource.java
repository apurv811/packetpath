/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebSer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 * REST Web Service
 *
 * @author Apurva Patel
 */
@Path("generic")
public class GenericResource {

    private static final String INPUT_FILE_NAME_1 = "C:\\Users\\Apurva Patel\\Documents\\NetBeansProjects\\WebApplication1\\";
    private static final String INPUT_FILE_NAME_2 = "C:\\Users\\Apurva Patel\\Documents\\NetBeansProjects\\WebApplication1\\";
    private static final int BUFFER_SIZE = 1024;
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of WebSer.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("{filename1}/{filename2}")
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public String getXml(@PathParam("filename1") String filename1,@PathParam("filename2") String filename2) {
        //TODO return proper representation object
        StringBuffer stringbuffer1 = new StringBuffer();
        StringBuffer stringbuffer2 = new StringBuffer();
        try{
            
            File file1 = new File(INPUT_FILE_NAME_1 + filename1);
            File file2 = new File(INPUT_FILE_NAME_2 + filename2);
            byte[] buffer1 = new byte[BUFFER_SIZE];
            byte[] buffer2 = new byte[BUFFER_SIZE];
            byte[] buffer_diff = new byte[16];
            int diffPointer = -1;
            int pointer = 0;
            
            RandomAccessFile raf1 = new RandomAccessFile(file1, "r");
            RandomAccessFile raf2 = new RandomAccessFile(file2, "r");
            
            //fc1.position(2);  // position to the byte you want to start reading
           // int len =0;
            
           long startTime = Calendar.getInstance().getTimeInMillis();
            int buff1Bytes, buff2Bytes;
            while((buff1Bytes = raf1.read(buffer1)) != -1 && (buff2Bytes = raf2.read(buffer2)) != -1 ){
                diffPointer = compare(buffer1, buffer2);
                if(diffPointer != -1 ) {
                    //System.out.println("Files are Different!");
                    //System.out.println("Differ From: " + (diffPointer + pointer));
                    //System.out.println("Next 16 Bytes: ");
                    stringbuffer1.append("Differ from: " + (diffPointer + pointer)+ "   Next 16 Bytes: ");
                    int remBytes = buff1Bytes - diffPointer;
                    if(remBytes < 16) {
                        byte[] remBuff = new byte[16  - remBytes];
                        int next = raf1.read(remBuff);
                        for(int i = diffPointer; i < buff1Bytes; i++){
                            stringbuffer1.append(buffer1[i] + " ");
                        }
                        if(next<16-remBytes){
                            for(int i = 0; i < next ; i++){
                                stringbuffer1.append(remBuff[i] + " ");
                            }
                            
                        }
                        else{
                           for(int i = 0; i < remBuff.length ; i++){
                                stringbuffer1.append(remBuff[i] + " ");
                            } 
                        }
                        
                    } else {
                        for(int i = diffPointer; i < diffPointer + 16; i++){
                            stringbuffer1.append(buffer1[i] + " ");
                        } 
                    }
                    break;
                }
                pointer += 1024;
               
                /*
                if(pointer > len) {
                    int newBuffSize = len - (pointer - 1024);
                    buffer1 = new byte[newBuffSize];
                    buffer2 = new byte[newBuffSize];
                } 
                */
                
            }
            if (diffPointer == -1) {
                stringbuffer1.append("Files are Same!");
            }
            
            stringbuffer2.append("Time Taken: " +(Calendar.getInstance().getTimeInMillis() - startTime)+ "ms");
        }
        
        catch (IOException ex) {
                ex.printStackTrace();
        }
        return "{'result':'" + stringbuffer1.toString() +"','time':'" + stringbuffer2.toString() +"'}";

    } 

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
            private static int compare(byte[] buffer1, byte[] buffer2) {
            int len =buffer1.length;
            for (int i=0 ; i < len ; i++) {
                if (buffer1[i] != buffer2[i]) {
                    return i;
                }
            }
            return -1;
        }
}
